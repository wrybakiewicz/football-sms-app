package notification

import com.nexmo.client.NexmoClient
import com.nexmo.client.auth.TokenAuthMethod
import com.nexmo.client.sms.messages.TextMessage
import notification.Message.{PhoneNumber, SenderName}

object Message {
  type SenderName = String
  type PhoneNumber = String
  val from = "FootballAPP"

  def apply(to: PhoneNumber, text: String): Message = new Message(from, to, text)
}

case class Message(from: SenderName, to: PhoneNumber, text: String)

class SmsSender() {

  private val auth = new TokenAuthMethod("12ff7a3f", "7c8c522065c6633a")
  private val client = new NexmoClient(auth).getSmsClient

  def send(message: Message): Either[String, Unit] = {
    val textMessage = new TextMessage(message.from, message.to, message.text)
    client.submitMessage(textMessage).toList.filter(x => x.getStatus != 0).map(_.getErrorText) match {
      case Nil => Right()
      case errors => Left(errors.mkString(" "))
    }
  }
}
