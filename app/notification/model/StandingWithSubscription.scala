package notification.model

import play.api.libs.json.{Json, OFormat}


object StandingWithSubscription {
  implicit val format: OFormat[StandingWithSubscription] = Json.format[StandingWithSubscription]
}

case class StandingWithSubscription(leagueId: String,
                               teamName: String,
                               leaguePosition: String,
                               matchPlayed: String,
                               matchWon: String,
                               matchDraw: String,
                               matchLost: String,
                               points: String,
                               isSubscribing: Boolean)
