package notification.model

case class SubscribeData(token: String, teamName: String)
