package notification.model

import authentication.dao.model.User
import football.dao.model.Event
import notification.Message

case class Notification(event: Event, subscribers: List[User]) {
  def message: String =
    s"${event.homeTeamName} vs ${event.awayTeamName} ${event.homeTeamScore}:${event.awayTeamScore} " +
      s"${event.matchDate} ${event.matchTime} ${event.countryName} ${event.leagueName}"

  def toMessages: List[Message] =
    subscribers.flatMap(_.phoneNumber).map(phoneNumber => Message(s"48$phoneNumber", message))
}
