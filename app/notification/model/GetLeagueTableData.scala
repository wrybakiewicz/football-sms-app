package notification.model

case class GetLeagueTableData(token: String, leagueId: String)
