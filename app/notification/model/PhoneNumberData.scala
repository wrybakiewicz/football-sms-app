package notification.model

case class PhoneNumberData(token: String, phoneNumber: String)
