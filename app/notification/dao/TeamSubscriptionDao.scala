package notification.dao

import authentication.dao.UserDao
import authentication.dao.model.User
import generic.{MongoDbConnector, Result, SubscriptionNotFound}
import javax.inject.Inject
import notification.dao.model.{TeamSubscription, TeamSubscriptionInsert, TeamSubscriptionUpdate}
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.{MongoCollection, _}
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Updates._

import scala.concurrent.{ExecutionContext, Future}

class TeamSubscriptionDao @Inject()(connector: MongoDbConnector, userDao: UserDao)(implicit val executionContext: ExecutionContext) {

  val collectionName: String = "teamSubscription"
  private val codecRegistry = fromRegistries(fromProviders(classOf[TeamSubscription]), DEFAULT_CODEC_REGISTRY, fromProviders(classOf[User]), DEFAULT_CODEC_REGISTRY)
  private val collection: MongoCollection[TeamSubscription] = connector.database.withCodecRegistry(codecRegistry).getCollection(collectionName)

  def insert(subscription: TeamSubscriptionInsert): Future[Result] = {
    val foundSubscriptionOption = findByTeamName(subscription.teamName)
    foundSubscriptionOption.flatMap {
      case Some(foundSubscription) =>
        if(!foundSubscription.subscribers.map(_.token).contains(subscription.user.token)) {
        val updatedSubscribers = subscription.user :: foundSubscription.subscribers
        collection.updateOne(equal("teamName", subscription.teamName), set("subscribers", updatedSubscribers)).toFuture().map(result => Result(result.toString))
        } else {
          Future.successful(Result(s"User ${subscription.user.name} has already subscribed team ${subscription.teamName}"))
        }
      case None =>
        val newSubscription = TeamSubscription(subscription.teamName, List(subscription.user))
        collection.insertOne(newSubscription).toFuture().map(result => Result(result.toString()))
    }
  }

  def updateSubscription(subscriptionUpdate: TeamSubscriptionUpdate): Future[Either[SubscriptionNotFound, Result]] = {
    findByTeamName(subscriptionUpdate.teamName) flatMap  {
      case Some(subscription) =>
        val updatedSubscribers = subscription.subscribers.filterNot(user => user._id == subscriptionUpdate.user._id)
        collection.updateOne(equal("teamName", subscription.teamName), set("subscribers", updatedSubscribers)).toFuture()
          .map(result => Right(Result(result.toString)))
      case None => Future.successful(Left(SubscriptionNotFound()))
    }

  }

  def isUserSubscribing(token: String, teamName: String): Future[Boolean] = {
    userDao.findByToken(token) flatMap {
      case Some(user) =>
        collection.find(equal("teamName", teamName)).headOption().map {
          case Some(subscription) =>
            subscription.subscribers.map(_._id).contains(user._id)
          case None => false
    }
      case None => Future.successful(false)
    }
  }

  def findByTeamName(teamName: String): Future[Option[TeamSubscription]] = {
    collection.find(equal("teamName", teamName)).headOption()
  }

}
