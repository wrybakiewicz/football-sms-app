package notification.dao.model

import authentication.dao.model.User

object TeamSubscription {
  def apply(teamName: String, subscribers: List[User]): TeamSubscription = new TeamSubscription(teamName, teamName, subscribers)
}
case class TeamSubscription(_id: String, teamName: String, subscribers: List[User])

case class TeamSubscriptionInsert(teamName: String, user: User)

case class TeamSubscriptionUpdate(teamName: String, user: User)