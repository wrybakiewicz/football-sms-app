package notification

import authentication.{AuthenticationHelper, AuthenticationService}
import football.dao.model.Standing
import generic.JsonSupport
import javax.inject.{Inject, _}
import notification.Message.PhoneNumber
import notification.model.{GetLeagueTableData, PhoneNumberData, StandingWithSubscription, SubscribeData}
import play.api.libs.json.{Json, OFormat, Reads}
import play.api.mvc.{AbstractController, ControllerComponents, _}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class TeamSubscriptionController @Inject()(cc: ControllerComponents, val authenticationService: AuthenticationService, teamSubscriptionService: TeamSubscriptionService)(implicit val executionContext: ExecutionContext)
  extends AbstractController(cc) with JsonSupport with AuthenticationHelper {

  implicit val subscribeDataReads: Reads[SubscribeData] = Json.reads[SubscribeData]
  implicit val phoneNumberReads: Reads[PhoneNumberData] = Json.reads[PhoneNumberData]
  implicit val getLeagueTableDataReads: Reads[GetLeagueTableData] = Json.reads[GetLeagueTableData]

  def subscribe(): Action[AnyContent] = Action.async { request =>
    request.body.asJson.map { json =>
      val data = json.as[SubscribeData]
      withTokenFuture(data.token) {
        teamSubscriptionService.subscribe(data).map {
          case Right(inserted) => Ok
          case Left(phoneNumberNotFound) => UnprocessableEntity
        }
      }
    }.getOrElse(Future.successful(BadRequest))
  }

  def unsubscribe(): Action[AnyContent] = Action.async { request =>
    request.body.asJson.map { json =>
      val data = json.as[SubscribeData]
      withTokenFuture(data.token) {
        teamSubscriptionService.unsubscribe(data).map {
          case Right(inserted) => Ok
          case Left(subscriptionNotFound) => NotFound
        }
      }
    }.getOrElse(Future.successful(BadRequest))
  }

  def savePhoneNumber(): Action[AnyContent] = Action.async { request =>
    request.body.asJson.map { json =>
      val data = json.as[PhoneNumberData]
      withTokenFuture(data.token) {
        authenticationService.addPhoneNumber(data).map(_ => Ok)
      }
    }.getOrElse(Future.successful(BadRequest))
  }

  def getLeagueTableWithSubscriptions: Action[AnyContent] = Action.async { request =>
    request.body.asJson.map { json =>
      val data = json.as[GetLeagueTableData]
      withTokenFuture(data.token) {
        teamSubscriptionService.getLeagueTableWithSubscriptions(data.leagueId, data.token).map(standing => Ok(Json.toJson(standing)))
      }
    }.getOrElse(Future.successful(BadRequest))
  }

}
