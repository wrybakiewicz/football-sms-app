package notification

import authentication.dao.UserDao
import football.FootballService
import football.dao.model.Event
import generic.{PhoneNumberNotFound, Result, SubscriptionNotFound}
import javax.inject.Inject
import notification.dao.TeamSubscriptionDao
import notification.dao.model.{TeamSubscriptionInsert, TeamSubscriptionUpdate}
import notification.model.{Notification, StandingWithSubscription, SubscribeData}
import play.api.Logger

import scala.concurrent.{ExecutionContext, Future}

class TeamSubscriptionService @Inject()(smsSender: SmsSender, footballService: FootballService, teamSubscriptionDao: TeamSubscriptionDao, userDao: UserDao)(implicit val executionContext: ExecutionContext) {

  def subscribe(subscribeData: SubscribeData): Future[Either[PhoneNumberNotFound, Result]] = {
    userDao.findByToken(subscribeData.token).flatMap {
      case Some(user) =>
        user.phoneNumber match {
          case Some(_) => teamSubscriptionDao.insert(TeamSubscriptionInsert(subscribeData.teamName, user)).map(Right(_))
          case None => Future.successful(Left(PhoneNumberNotFound()))
        }
      case None => sys.error(s"User with token ${subscribeData.token} could not be found")
    }
  }

  def unsubscribe(subscribeData: SubscribeData): Future[Either[SubscriptionNotFound, Result]] = {
    userDao.findByToken(subscribeData.token).flatMap {
      case Some(user) =>
        user.phoneNumber match {
          case Some(_) => teamSubscriptionDao.updateSubscription(TeamSubscriptionUpdate(subscribeData.teamName, user))
          case None => sys.error(s"User with token ${subscribeData.token} has no phone number")
        }
      case None => sys.error(s"User with token ${subscribeData.token} could not be found")
    }
  }

  def getLeagueTableWithSubscriptions(leagueId: String, token: String): Future[Seq[StandingWithSubscription]] = {
    footballService.getStandings(leagueId).flatMap { standings =>
      Future.sequence(standings.map(standing => {
        teamSubscriptionDao.isUserSubscribing(token, standing.teamName) map { isSubscribing =>
          StandingWithSubscription(
            standing.leagueId,
            standing.teamName,
            standing.leaguePosition,
            standing.matchPlayed,
            standing.matchWon,
            standing.matchDraw,
            standing.matchLost,
            standing.points,
            isSubscribing)
        }
      }))
    }
  }

  def notifySubscribers(events: List[Event]): Future[List[Unit]] = {
    val matchFinishedStatus = "FT"
    Future.sequence(events.filter(event => event.matchStatus == matchFinishedStatus)
      .map(createNotification))
      .map(notifications => notifications.flatMap(_.toMessages))
      .map(messages => messages.map(smsSender.send))
      .map(results => results.map {
        case Right(_) => ()
        case Left(error) => Logger.error(s"Error sending sms $error")
      })
  }

  private def createNotification(event: Event): Future[Notification] = {
    for {
    homeTeamSubscribers <- teamSubscriptionDao.findByTeamName(event.homeTeamName).map(subscription => subscription.map(_.subscribers).toList.flatten)
    awayTeamSubscribers <- teamSubscriptionDao.findByTeamName(event.awayTeamName).map(subscription => subscription.map(_.subscribers).toList.flatten)
    } yield {
      val subscribers = (homeTeamSubscribers ++ awayTeamSubscribers).distinct
      Notification(event, subscribers)
    }
  }

}
