package authentication.dao.model

import play.api.libs.json.{Json, OFormat}

case class User(_id: String, token: String, name: String, isLogged: Boolean, phoneNumber: Option[String]) {
  implicit val format: OFormat[User] = Json.format[User]
}