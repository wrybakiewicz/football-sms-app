package authentication.dao

import akka.actor.ActorSystem
import authentication.dao.model.User
import generic.MongoDbConnector
import javax.inject.Inject
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala._
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.UpdateOptions
import org.mongodb.scala.model.Updates._
import org.mongodb.scala.result.UpdateResult

import scala.concurrent.{ExecutionContext, Future}

class UserDao @Inject()(connector: MongoDbConnector)(implicit val system: ActorSystem, implicit val executionContext: ExecutionContext) {

  val collectionName: String = "users"
  private val codecRegistry = fromRegistries(fromProviders(classOf[User]), DEFAULT_CODEC_REGISTRY)
  private val collection: MongoCollection[User] = connector.database.withCodecRegistry(codecRegistry).getCollection(collectionName)

  def insertOrUpdate(user: User): Future[UpdateResult] = {
      val params = List(
        set("token", user.token),
        set("name", user.name),
        set("isLogged", user.isLogged)) ++ user.phoneNumber.map(number => set("phoneNumber", number)).toList
    collection.updateOne(equal("_id", user._id), combine(params: _*),
      new UpdateOptions().upsert(true)).toFuture()
  }

  def findByToken(token: String): Future[Option[User]] = collection.find(equal("token", token)).toFuture().map(_.headOption)

  def updateIsLogged(token: String, isLogged: Boolean): Future[UpdateResult] =
    collection.updateOne(equal("token", token), set("isLogged", isLogged)).toFuture()

  def updatePhoneNumber(token: String, phoneNumber: String): Future[UpdateResult] =
    collection.updateOne(equal("token", token), set("phoneNumber", phoneNumber)).toFuture()

}
