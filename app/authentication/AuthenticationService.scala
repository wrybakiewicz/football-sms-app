package authentication

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpRequest, HttpResponse, StatusCodes}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.Materializer
import authentication.dao.UserDao
import authentication.dao.model.User
import authentication.model.{FacebookLoginData, FacebookResponseData}
import generic.{Fail, JsonSupport}
import javax.inject.Inject
import notification.model.PhoneNumberData
import org.mongodb.scala.result.UpdateResult
import play.api.Logger

import scala.concurrent.{ExecutionContext, Future}

class AuthenticationService @Inject()(userDao: UserDao)(implicit val system: ActorSystem, implicit val materializer: Materializer, implicit val executionContext: ExecutionContext) extends JsonSupport {

  private val authenticationUrl = "https://graph.facebook.com/me?access_token="

  def login(facebookData: FacebookLoginData): Future[Either[Fail, Unit]] = {
    authenticateRequest(facebookData.token) flatMap {
      case Right(responseData) =>
        if(responseData.id != facebookData.id) Future.successful(Left(Fail("Id does not match with token")))
        else {
          userDao.insertOrUpdate(User(facebookData.id, facebookData.token, facebookData.name, isLogged = true, None))
            .map(response => {
              Logger.info(s"authenticate userDao.insertOrUpdate user ${responseData.name}: $response")
              Right(())
            })
        }
      case Left(fail) => Future.successful(Left(fail))
    }
  }

  def authenticate(token: String): Future[Option[User]] = {
    userDao.findByToken(token)
  }

  def logout(token: String): Future[Unit] = {
    userDao.updateIsLogged(token, isLogged = false).map(result => {
      Logger.info(s"logout userDao.delete user with token: $token result: $result")
      ()
    })
  }

  def addPhoneNumber(phoneNumberData: PhoneNumberData): Future[Unit] =
    userDao.updatePhoneNumber(phoneNumberData.token, phoneNumberData.phoneNumber).map { result =>
      Logger.info(s"addPhoneNumber with $phoneNumberData result: $result")
      ()
    }

  private def authenticateRequest(token: String): Future[Either[Fail, FacebookResponseData]] = {
    Logger.info(s"Request to authenticate with token: $token")
    Http().singleRequest(HttpRequest(uri = s"$authenticationUrl$token")).flatMap {
      case HttpResponse(StatusCodes.OK, _, entity, _) =>
        Unmarshal(entity.withContentType(ContentTypes.`application/json`)).to[FacebookResponseData].map(Right(_))
      case _ => Future.successful(Left(Fail("Request failed")))
    }
  }
}
