package authentication

import authentication.model.{FacebookLoginData, FacebookLogoutData}
import generic.JsonSupport
import javax.inject.{Inject, _}
import play.api.libs.json.{Json, Reads}
import play.api.mvc.{AbstractController, ControllerComponents, _}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AuthenticationController @Inject()(cc: ControllerComponents, authenticationService: AuthenticationService)(implicit val executionContext: ExecutionContext)
  extends AbstractController(cc) with JsonSupport {

  implicit val loginDataReads: Reads[FacebookLoginData] = Json.reads[FacebookLoginData]
  implicit val logoutDataReads: Reads[FacebookLogoutData] = Json.reads[FacebookLogoutData]

  def login: Action[AnyContent] = Action.async { request =>
    request.body.asJson.map { json =>
      val data = json.as[FacebookLoginData]
      authenticationService.login(data) map {
        case Right(_) => Ok
        case Left(fail) => BadRequest(fail.message)
      }
    }.getOrElse(Future.successful(BadRequest))
  }

  def logout: Action[AnyContent] = Action.async { request =>
    request.body.asJson.map { json =>
      val data = json.as[FacebookLogoutData]
      authenticationService.logout(data.token) map (_ => Ok)
    }.getOrElse(Future.successful(BadRequest))
  }

}
