package authentication.model

case class FacebookLoginData(id: String, name: String, token: String)

case class FacebookLogoutData(token: String)

case class FacebookResponseData(id: String, name: String)