package authentication

import play.api.mvc.{Result, Results}

import scala.concurrent.{ExecutionContext, Future}

trait AuthenticationHelper {

  val authenticationService: AuthenticationService
  implicit val executionContext: ExecutionContext

  def withTokenFuture(token: String)(fun: => Future[Result]): Future[Result] = {
    authenticationService.authenticate(token) flatMap {
      case Some(user) =>
        if(user.isLogged && user.token == token) {
          fun
        } else {
          Future.successful(Results.Unauthorized)
        }
      case None => Future.successful(Results.Unauthorized)
    }
  }

}
