package football.api

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, _}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.{ActorMaterializer, Materializer}
import football.api.model._
import javax.inject.Inject
import generic.{Fail, JsonSupport}
import play.api.Logger

import scala.concurrent.{ExecutionContext, Future}

class FootballApiRetriver @Inject()(implicit val system: ActorSystem, implicit val materializer: Materializer, implicit val executionContext: ExecutionContext) extends JsonSupport {

  val apiUrl = "https://apifootball.com/api/"
  val appKey = "bb351f9a3a155f274db3fc3ef497e623ceaac11fb37d3172b6464dbedb151562"

  def getAllCountries: Future[Either[Fail, List[CountryApi]]] = {
    Logger.info("Request getCountries")
    Http().singleRequest(HttpRequest(uri = s"$apiUrl?action=get_countries&APIkey=$appKey")).flatMap {
      case HttpResponse(StatusCodes.OK, _, entity, _) =>
        Unmarshal(entity).to[List[CountryApi]].map(Right(_))
      case _ => Future.successful(Left(Fail("Request failed")))
    }
  }

  def getLeagues(countryId: String): Future[Either[Fail, List[LeagueApi]]] = {
    Logger.info(s"Request getLeagues: $countryId")
    Http().singleRequest(HttpRequest(uri = s"$apiUrl?action=get_leagues&country_id=$countryId&APIkey=$appKey")).flatMap {
      case HttpResponse(StatusCodes.OK, _, entity, _) =>
        Unmarshal(entity).to[List[LeagueApi]].map(Right(_))
      case _ => Future.successful(Left(Fail("Request failed")))
    }
  }

  def getStandings(leagueId: String): Future[Either[Fail, List[StandingApi]]] = {
    Logger.info(s"Request getStandings: $leagueId")
    Http().singleRequest(HttpRequest(uri = s"$apiUrl?action=get_standings&league_id=$leagueId&APIkey=$appKey")).flatMap {
      case HttpResponse(StatusCodes.OK, _, entity, _) =>
        Unmarshal(entity).to[List[StandingApi]].map(Right(_))
      case _ => Future.successful(Left(Fail("Request failed")))
    }
  }

  def getEvents(leagueId: String, from: LocalDate, to: LocalDate): Future[Either[Fail, List[EventApi]]] = {
    Logger.info(s"Request getEvents: $leagueId $from $to")
    Http().singleRequest(HttpRequest(uri = s"$apiUrl?action=get_events&from=${formatDate(from)}&to=${formatDate(to)}&league_id=$leagueId&APIkey=$appKey")).flatMap {
      case HttpResponse(StatusCodes.OK, _, entity, _) =>
        Unmarshal(entity).to[List[EventApi]].map(Right(_))
      case HttpResponse(code, _, entity, _) =>
        Unmarshal(entity).to[String].map(response => Left(Fail(s"Request failed: $code $response")))
    }
  }

  private def formatDate(date: LocalDate) = date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
}
