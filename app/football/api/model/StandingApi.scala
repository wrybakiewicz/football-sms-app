package football.api.model

import football.dao.model.Standing
import org.mongodb.scala._

case class StandingApi(league_id: String,
                       team_name: String,
                       overall_league_position: String,
                       overall_league_payed: String,
                       overall_league_W: String,
                       overall_league_D: String,
                       overall_league_L: String,
                       overall_league_PTS: String) {

  def toStanding: Standing =
    Standing(league_id,
      team_name,
      overall_league_position,
      overall_league_payed,
      overall_league_W,
      overall_league_D,
      overall_league_L,
      overall_league_PTS)

}
