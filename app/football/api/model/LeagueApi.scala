package football.api.model

import football.dao.model.League

case class LeagueApi(league_id: String,
                     league_name: String,
                     country_id: String,
                     country_name: String) {

  def toLeague: League = League(league_id, league_name, country_id, country_name)
}
