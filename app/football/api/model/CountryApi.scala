package football.api.model

import football.dao.model.Country

case class CountryApi(country_id: String,
                      country_name: String) {
  def toCountry: Country = Country(country_id, country_name)
}