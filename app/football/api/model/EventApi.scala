package football.api.model

import football.dao.model.Event

case class EventApi(match_id: String,
                    match_date: String,
                    match_time: String,
                    country_id: String,
                    country_name: String,
                    league_id: String,
                    league_name: String,
                    match_hometeam_name: String,
                    match_awayteam_name: String,
                    match_hometeam_score: String,
                    match_awayteam_score: String,
                    match_status: String) {

  def toEvent: Event =
    Event(match_id,
      match_date,
      match_time,
      country_id,
      country_name,
      league_id,
      league_name,
      match_hometeam_name,
      match_awayteam_name,
      match_hometeam_score,
      match_awayteam_score,
      match_status)
}