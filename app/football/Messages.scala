package football

object Messages extends Enumeration {
  val saveCountries, saveLeagues, saveStandings, saveEvents = Value
}
