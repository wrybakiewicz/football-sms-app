package football

import authentication.{AuthenticationHelper, AuthenticationService}
import javax.inject.{Inject, _}
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents, _}

import scala.concurrent.ExecutionContext

@Singleton
class FootballController @Inject()(cc: ControllerComponents, footballService: FootballService, scheduler: Scheduler, val authenticationService: AuthenticationService)(implicit val executionContext: ExecutionContext) extends AbstractController(cc) with AuthenticationHelper {

  def getCountries(token: String): Action[AnyContent] = Action.async {
    withTokenFuture(token) {
      footballService.getCountries.map(countries => Ok(Json.toJson(countries)))
    }
  }

  def getLeagues(countryId: String, token: String): Action[AnyContent] = Action.async {
    withTokenFuture(token) {
      footballService.getLeagues(countryId).map(leagues => Ok(Json.toJson(leagues)))
    }
  }

}