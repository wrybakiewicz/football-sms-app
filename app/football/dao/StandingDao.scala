package football.dao

import akka.actor.ActorSystem
import football.dao.model.Standing
import generic.MongoDbConnector
import javax.inject.Inject
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala._
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Updates._
import org.mongodb.scala.result.{DeleteResult, UpdateResult}

import scala.concurrent.{ExecutionContext, Future}

class StandingDao @Inject()(connector: MongoDbConnector)(implicit val system: ActorSystem, implicit val executionContext: ExecutionContext) {

  val collectionName: String = "standings"
  private val codecRegistry = fromRegistries(fromProviders(classOf[Standing]), DEFAULT_CODEC_REGISTRY)
  private val collection: MongoCollection[Standing] = connector.database.withCodecRegistry(codecRegistry).getCollection(collectionName)

  def insert(standings: Seq[Standing]): Future[Completed] = collection.insertMany(standings).toFuture()

  def update(standings: Seq[Standing]): Future[Seq[UpdateResult]] = {
    val updates = standings.map(standing =>
      collection.updateOne(equal("_id", standing._id),
        combine(
          set("teamName", standing.teamName),
          set("leaguePosition", standing.leaguePosition),
          set("matchPlayed", standing.matchPlayed),
          set("matchWon", standing.matchWon),
          set("matchDraw", standing.matchDraw),
          set("matchLost", standing.matchLost),
          set("points", standing.points))).toFuture())
    Future.sequence(updates)
  }

  def delete(standings: Seq[Standing]): Future[Seq[DeleteResult]] = {
    val deletes = standings.map(standing => collection.deleteOne(equal("_id", standing._id)).toFuture())
    Future.sequence(deletes)
  }

  def getAll: Future[Seq[Standing]] = collection.find().toFuture()

  def getByLeagueId(leagueId: String): Future[Seq[Standing]] = collection.find(equal("leagueId", leagueId)).toFuture()

  def drop: Future[Completed] = collection.drop().toFuture()

}
