package football.dao

import akka.actor.ActorSystem
import football.dao.model.Country
import generic.MongoDbConnector
import javax.inject.Inject
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala._
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Updates._
import org.mongodb.scala.result.{DeleteResult, UpdateResult}

import scala.concurrent.{ExecutionContext, Future}

class CountryDao @Inject()(connector: MongoDbConnector)(implicit val system: ActorSystem, implicit val executionContext: ExecutionContext) {

  val collectionName: String = "countries"
  private val codecRegistry = fromRegistries(fromProviders(classOf[Country]), DEFAULT_CODEC_REGISTRY)
  private val collection: MongoCollection[Country] = connector.database.withCodecRegistry(codecRegistry).getCollection(collectionName)

  def insert(countries: Seq[Country]): Future[Completed] = collection.insertMany(countries).toFuture()

  def update(countries: Seq[Country]): Future[Seq[UpdateResult]] = {
    val updates = countries.map(country =>
      collection.updateOne(equal("_id", country._id), set("countryName", country.countryName)).toFuture())
    Future.sequence(updates)
  }

  def delete(countries: Seq[Country]): Future[Seq[DeleteResult]] = {
    val deletes = countries.map(country => collection.deleteOne(equal("_id", country._id)).toFuture())
    Future.sequence(deletes)
  }

  def getAll: Future[Seq[Country]] = collection.find().toFuture()

  def drop: Future[Completed] = collection.drop().toFuture()

}
