package football.dao.model

import play.api.Logger

object Event {
  def apply(matchId: String,
            matchDate: String,
            matchTime: String,
            countryId: String,
            countryName: String,
            leagueId: String,
            leagueName: String,
            homeTeamName: String,
            awayTeamName: String,
            homeTeamScore: String,
            awayTeamScore: String,
            matchStatus: String): Event =
    new Event(matchId, matchId, matchDate, matchTime, countryId, countryName, leagueId, leagueName, homeTeamName, awayTeamName, homeTeamScore, awayTeamScore, matchStatus)
}

case class Event(_id: String,
                 matchId: String,
                 matchDate: String,
                 matchTime: String,
                 countryId: String,
                 countryName: String,
                 leagueId: String,
                 leagueName: String,
                 homeTeamName: String,
                 awayTeamName: String,
                 homeTeamScore: String,
                 awayTeamScore: String,
                 matchStatus: String) {

  override def hashCode(): Int = this._id.hashCode

  override def canEqual(that: Any): Boolean = that.isInstanceOf[Event]

  override def equals(obj: Any): Boolean = obj match {
    case that: Event => that.canEqual(this) && that._id == this._id
    case _ => false
  }

  def isTheSame(that: Event): Boolean = {
    val theSame = this._id == that._id &&
      this.matchId == that.matchId &&
      this.matchDate == that.matchDate &&
      this.matchTime == that.matchTime &&
      this.countryId == that.countryId &&
      this.countryName == that.countryName &&
      this.leagueId == that.leagueId &&
      this.leagueName == that.leagueName &&
      this.homeTeamName == that.homeTeamName &&
      this.awayTeamName == that.awayTeamName &&
      this.homeTeamScore == that.homeTeamScore &&
      this.awayTeamScore == that.awayTeamScore &&
      this.matchStatus == that.matchStatus

    if (!theSame) {
      Logger.info(s"Changed Standing is: ${this.toString} was: $that")
    }

    theSame
  }
}