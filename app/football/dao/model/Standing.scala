package football.dao.model

import play.api.libs.json.{Json, OFormat}

object Standing {
  def apply(leagueId: String,
            teamName: String,
            leaguePosition: String,
            matchPlayed: String,
            matchWon: String,
            matchDraw: String,
            matchLost: String,
            points: String): Standing = new Standing(s"${leagueId}_$teamName", leagueId, teamName, leaguePosition, matchPlayed, matchWon, matchDraw, matchLost, points)

  implicit val format: OFormat[Standing] = Json.format[Standing]

}

case class Standing(_id: String,
                    leagueId: String,
                    teamName: String,
                    leaguePosition: String,
                    matchPlayed: String,
                    matchWon: String,
                    matchDraw: String,
                    matchLost: String,
                    points: String) {

  override def hashCode(): Int = this._id.hashCode

  override def canEqual(that: Any): Boolean = that.isInstanceOf[Standing]

  override def equals(obj: Any): Boolean = obj match {
    case that: Standing => that.canEqual(this) && that._id == this._id
    case _ => false
  }

  def isTheSame(that: Standing): Boolean =
    this._id == that._id &&
      this.leagueId == that.leagueId &&
      this.teamName == that.teamName &&
      this.leaguePosition == that.leaguePosition &&
      this.matchPlayed == that.matchPlayed &&
      this.matchWon == that.matchWon &&
      this.matchDraw == that.matchDraw &&
      this.matchLost == that.matchLost &&
      this.points == that.points
}
