package football.dao.model

import play.api.libs.json.{Json, OFormat}

object Country {
  def apply(countryId: String,
            countryName: String): Country = new Country(countryId, countryId, countryName)

  implicit val format: OFormat[Country] = Json.format[Country]
}

case class Country(_id: String, countryId: String, countryName: String) {
  override def hashCode(): Int = this._id.hashCode

  override def canEqual(that: Any): Boolean = that.isInstanceOf[Country]

  override def equals(obj: Any): Boolean = obj match {
    case that: Country => that.canEqual(this) && that._id == this._id
    case _ => false
  }

  def isTheSame(that: Country): Boolean =
    this._id == that._id &&
    this.countryId == that.countryId &&
      this.countryName == that.countryName
}