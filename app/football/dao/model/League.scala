package football.dao.model

import play.api.libs.json.{Json, OFormat}

object League {
  def apply(leagueId: String,
            leagueName: String,
            countryId: String,
            countryName: String): League = new League(leagueId, leagueId, leagueName, countryId, countryName)

  implicit val format: OFormat[League] = Json.format[League]
}

case class League(_id: String,
                  leagueId: String,
                  leagueName: String,
                  countryId: String,
                  countryName: String) {

  override def hashCode(): Int = this._id.hashCode

  override def canEqual(that: Any): Boolean = that.isInstanceOf[League]

  override def equals(obj: Any): Boolean = obj match {
    case that: League => that.canEqual(this) && that._id == this._id
    case _ => false
  }

  def isTheSame(that: League): Boolean =
    this._id == that._id &&
      this.leagueId == that.leagueId &&
      this.leagueName == that.leagueName &&
      this.countryId == that.countryId &&
      this.countryName == that.countryName
}