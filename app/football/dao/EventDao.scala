package football.dao

import akka.actor.ActorSystem
import football.dao.model.Event
import generic.MongoDbConnector
import javax.inject.Inject
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala._
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Updates._
import org.mongodb.scala.result.{DeleteResult, UpdateResult}

import scala.concurrent.{ExecutionContext, Future}

class EventDao @Inject()(connector: MongoDbConnector)(implicit val system: ActorSystem, implicit val executionContext: ExecutionContext) {

  val collectionName: String = "events"
  private val codecRegistry = fromRegistries(fromProviders(classOf[Event]), DEFAULT_CODEC_REGISTRY)
  private val collection: MongoCollection[Event] = connector.database.withCodecRegistry(codecRegistry).getCollection(collectionName)

  def insert(events: Seq[Event]): Future[Completed] =
    collection.insertMany(events).toFuture()

  def update(events: Seq[Event]): Future[Seq[UpdateResult]] = {
    val updates = events.map(event =>
      collection.updateOne(equal("_id", event._id),
        combine(
          set("matchDate", event.matchDate),
          set("matchTime", event.matchTime),
          set("countryId", event.countryId),
          set("countryName", event.countryName),
          set("leagueId", event.leagueId),
          set("leagueName", event.leagueName),
          set("homeTeamName", event.homeTeamName),
          set("awayTeamName", event.awayTeamName),
          set("homeTeamScore", event.homeTeamScore),
          set("awayTeamScore", event.awayTeamScore),
          set("matchStatus", event.matchStatus))).toFuture())
    Future.sequence(updates)
  }

  def delete(events: Seq[Event]): Future[Seq[DeleteResult]] = {
    val deletes = events.map(country => collection.deleteOne(equal("_id", country._id)).toFuture())
    Future.sequence(deletes)
  }

  def getAll: Future[Seq[Event]] = collection.find().toFuture()

  def drop: Future[Completed] = collection.drop().toFuture()

}
