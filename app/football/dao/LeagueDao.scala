package football.dao

import akka.actor.ActorSystem
import football.dao.model.League
import generic.MongoDbConnector
import javax.inject.Inject
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala._
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Updates._
import org.mongodb.scala.result.{DeleteResult, UpdateResult}

import scala.concurrent.{ExecutionContext, Future}

class LeagueDao @Inject()(connector: MongoDbConnector)(implicit val system: ActorSystem, implicit val executionContext: ExecutionContext) {

  val collectionName = "leagues"
  private val codecRegistry = fromRegistries(fromProviders(classOf[League]), DEFAULT_CODEC_REGISTRY)
  private val collection: MongoCollection[League] = connector.database.withCodecRegistry(codecRegistry).getCollection(collectionName)

  def insert(leagues: Seq[League]): Future[Completed] = collection.insertMany(leagues).toFuture()

  def update(leagues: Seq[League]): Future[Seq[UpdateResult]] = {
    val updates = leagues.map(league =>
      collection.updateOne(equal("_id", league._id),
        combine(
          set("leagueName", league.leagueName),
          set("countryId", league.countryId),
          set("countryName", league.countryName))).toFuture())
    Future.sequence(updates)
  }

  def delete(leagues: Seq[League]): Future[Seq[DeleteResult]] = {
    val deletes = leagues.map(league => collection.deleteOne(equal("_id", league._id)).toFuture())
    Future.sequence(deletes)
  }

  def getAll: Future[Seq[League]] = collection.find().toFuture()

  def getByCountryId(countryId: String): Future[Seq[League]] = collection.find(equal("countryId", countryId)).toFuture()

  def drop: Future[Completed] = collection.drop().toFuture()

}
