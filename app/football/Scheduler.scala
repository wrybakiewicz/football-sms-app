package football

import akka.actor.{ActorRef, ActorSystem, Props}
import dao.{CountryDao, EventDao, LeagueDao, StandingDao}
import api.FootballApiRetriver
import javax.inject.Inject
import notification.TeamSubscriptionService

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class Scheduler @Inject()(system: ActorSystem,
                          countryDao: CountryDao,
                          leagueDao: LeagueDao,
                          standingDao: StandingDao,
                          eventDao: EventDao,
                          apiRetriver: FootballApiRetriver,
                          teamSubscriptionService: TeamSubscriptionService)(implicit val executionContext: ExecutionContext) {

  val saverActor: ActorRef = system.actorOf(Props(classOf[FootballSaverActor], countryDao, leagueDao, standingDao, eventDao, apiRetriver, teamSubscriptionService, system, system.dispatcher))

  system.scheduler.schedule(
    initialDelay = 0 second,
    interval = 1 hour,
    receiver = saverActor,
    message = Messages.saveCountries
  )

  system.scheduler.schedule(
    initialDelay = 10 second,
    interval = 1 hour,
    receiver = saverActor,
    message = Messages.saveLeagues
  )

  system.scheduler.schedule(
    initialDelay = 20 second,
    interval = 30 minutes,
    receiver = saverActor,
    message = Messages.saveStandings
  )

  system.scheduler.schedule(
    initialDelay = 40 second,
    interval = 10 minutes,
    receiver = saverActor,
    message = Messages.saveEvents
  )
}
