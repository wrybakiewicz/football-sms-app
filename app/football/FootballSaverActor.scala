package football

import java.time.LocalDate

import akka.actor.{Actor, ActorSystem}
import dao.{CountryDao, EventDao, LeagueDao, StandingDao}
import api.FootballApiRetriver
import notification.TeamSubscriptionService
import play.api.Logger

import scala.concurrent.{ExecutionContext, Future}

class FootballSaverActor(countryDao: CountryDao,
                         leagueDao: LeagueDao,
                         standingDao: StandingDao,
                         eventDao: EventDao,
                         apiRetriver: FootballApiRetriver,
                         teamSubscriptionService: TeamSubscriptionService)(implicit val system: ActorSystem, implicit val executionContext: ExecutionContext) extends Actor {

  private val dateFrom: LocalDate = LocalDate.now.minusMonths(6)
  private val dateTo: LocalDate = LocalDate.now.plusMonths(6)

  override def receive: Receive = {
    case Messages.saveCountries =>
      Logger.info("Received saveCountries")
      this.saveCountries()
    case Messages.saveLeagues =>
      Logger.info("Received saveLeagues")
      this.saveLeagues()
    case Messages.saveEvents =>
      Logger.info("Received saveEvents")
      this.saveEvents()
    case Messages.saveStandings =>
      Logger.info("Received saveStandings")
      this.saveStandings()
  }

  def saveCountries(): Future[Unit] =
    (for {
      apiCountriesEither <- apiRetriver.getAllCountries
      savedCountries <- countryDao.getAll
    } yield {
      apiCountriesEither match {
        case Right(apiCountries) =>
          Logger.info(s"saveCountries apiRetriver.getCountries: ${apiCountries.size}")
          Logger.info(s"saveCountries countryDao.getAll: ${savedCountries.size}")
          val mappedApiCountries = apiCountries.map(_.toCountry)
          val insert = mappedApiCountries.diff(savedCountries) match {
            case Nil => Future.successful()
            case elements =>
              Logger.info(s"saveCountries countryDao.insert: size: ${elements.size}")
              countryDao.insert(elements).map(completed => Logger.info(s"saveCountries countryDao.insert: size: ${elements.size} result: ${completed.toString()}"))
          }
          val update = mappedApiCountries.filter(mappedCountry => savedCountries.exists(savedCountry => mappedCountry == savedCountry && !mappedCountry.isTheSame(savedCountry))) match {
            case Nil => Future.successful()
            case elements =>
              Logger.info(s"saveCountries countryDao.update: size: ${elements.size}")
              countryDao.update(elements).map(updateResults => Logger.info(s"saveCountries countryDao.update: size: ${elements.size} result: ${updateResults.mkString(",")}"))
          }
          val delete = savedCountries.diff(mappedApiCountries) match {
            case Nil => Future.successful()
            case elements =>
              Logger.info(s"saveCountries countryDao.delete: size: ${elements.size}")
              countryDao.delete(elements).map(deleteResults => Logger.info(s"saveCountries countryDao.delete: size: ${elements.size} result: ${deleteResults.mkString(",")}"))
          }
          Future.sequence(insert :: update :: delete :: Nil).map(_ => ())
        case Left(fail) =>
          Future.successful(Logger.warn(s"saveCountries apiRetriver.getCountries failed: $fail"))
      }
    }).flatten

  def saveLeagues(): Future[Unit] = {
    val ignoredLeagueIds = List("804", "805", "994", "995", "1022", "1023")
    (for {
      savedCountries <- countryDao.getAll
      savedLeagues <- leagueDao.getAll
    } yield {
      val resultsApi = savedCountries.map {
        savedCountry =>
          apiRetriver.getLeagues(savedCountry.countryId) map {
            case Right(apiLeagues) =>
              Logger.info(s"saveLeagues apiRetriver.apiRetriver.getLeague(${savedCountry.countryId}) result: ${apiLeagues.size}")
              Logger.info(s"saveLeagues ignoring leagues with id: ${apiLeagues.filter(league => ignoredLeagueIds.contains(league.league_id)).map(_.league_id)}")
              apiLeagues.filterNot(league => ignoredLeagueIds.contains(league.league_id))
            case Left(fail) =>
              Logger.warn(s"saveLeagues apiRetriver.apiRetriver.getLeague(${savedCountry.countryId}) failed: $fail")
              List.empty
          }
      }.toList
      Future.sequence(resultsApi).map(_.flatten).flatMap { apiLeagues =>
        val mappedApiLeagues = apiLeagues.map(_.toLeague)
        val insert = mappedApiLeagues.diff(savedLeagues) match {
          case Nil => Future.successful()
          case elements =>
            Logger.info(s"saveLeagues leagueDao.insert: size: ${elements.size}")
            leagueDao.insert(elements).map(completed => Logger.info(s"saveLeagues leagueDao.insert: size: ${elements.size} result: ${completed.toString()}"))
        }
        val update = mappedApiLeagues.filter(mappedLeague => savedLeagues.exists(savedLeague => mappedLeague == savedLeague && !mappedLeague.isTheSame(savedLeague))) match {
          case Nil => Future.successful()
          case elements =>
            Logger.info(s"saveLeagues leagueDao.update: size: ${elements.size}")
            leagueDao.update(elements).map(updateResults => Logger.info(s"saveLeagues leagueDao.update: size: ${elements.size} result: ${updateResults.mkString(",")}"))
        }
        val delete = savedLeagues.diff(mappedApiLeagues) match {
          case Nil => Future.successful()
          case elements =>
            Logger.info(s"saveLeagues leagueDao.delete: size: ${elements.size}")
            leagueDao.delete(elements).map(deleteResults => Logger.info(s"saveLeagues leagueDao.delete: size: ${elements.size} result: ${deleteResults.mkString(",")}"))
        }
        Future.sequence(insert :: update :: delete :: Nil).map(_ => ())
      }
    }).flatten
  }

  def saveStandings(): Future[Unit] =
    (for {
      savedLeagues <- leagueDao.getAll
      savedStandings <- standingDao.getAll
    } yield {
      val resultsApi = savedLeagues.map {
        league =>
          apiRetriver.getStandings(league.leagueId) map {
            case Right(apiStandings) =>
              Logger.info(s"saveStandings apiRetriver.apiRetriver.getStandings(${league.leagueId}) result: ${apiStandings.size}")
              apiStandings
            case Left(fail) =>
              Logger.warn(s"saveStandings apiRetriver.apiRetriver.getStandings(${league.leagueId}) failed: $fail")
              List.empty
          }
      }.toList
      Future.sequence(resultsApi).map(_.flatten).flatMap { apiStandings =>
        val mappedApiStandings = apiStandings.map(_.toStanding)
        val insert = mappedApiStandings.diff(savedStandings) match {
          case Nil => Future.successful()
          case elements =>
            Logger.info(s"saveStandings standingDao.insert: size: ${elements.size}")
            standingDao.insert(elements).map(completed => Logger.info(s"saveStandings standingDao.insert: size: ${elements.size} result: ${completed.toString()}"))
        }
        val update = mappedApiStandings.filter(mappedStanding => savedStandings.exists(savedStanding => mappedStanding == savedStanding && !mappedStanding.isTheSame(savedStanding))) match {
          case Nil => Future.successful()
          case elements =>
            Logger.info(s"saveStandings standingDao.update: size: ${elements.size}")
            standingDao.update(elements).map(updateResults => Logger.info(s"saveStandings standingDao.update: size: ${elements.size} result: ${updateResults.mkString(",")}"))
        }
        val delete = savedStandings.diff(mappedApiStandings) match {
          case Nil => Future.successful()
          case elements =>
            Logger.info(s"saveStandings standingDao.delete: size: ${elements.size}")
            standingDao.delete(elements).map(deleteResults => Logger.info(s"saveStandings standingDao.delete: size: ${elements.size} result: ${deleteResults.mkString(",")}"))
        }
        Future.sequence(insert :: update :: delete :: Nil).map(_ => ())
      }
    }).flatten

  def saveEvents(): Future[Unit] =
    (for {
      savedLeagues <- leagueDao.getAll
      savedEvents <- eventDao.getAll
    } yield {
      val resultsApi = savedLeagues.map {
        league =>
          apiRetriver.getEvents(league.leagueId, dateFrom, dateTo) map {
            case Right(apiEvents) =>
              Logger.info(s"saveEvents apiRetriver.apiRetriver.getEvents(${league.leagueId}, $dateFrom, $dateTo) result: ${apiEvents.size}")
              apiEvents
            case Left(fail) =>
              Logger.warn(s"saveEvents apiRetriver.apiRetriver.getEvents(${league.leagueId}, $dateFrom, $dateTo) failed: $fail")
              List.empty
          }
      }.toList
      Future.sequence(resultsApi).map(_.flatten).flatMap { apiEvents =>
        val mappedApiEvents = apiEvents.map(_.toEvent)
        val insert = mappedApiEvents.diff(savedEvents) match {
          case Nil => Future.successful()
          case elements =>
            teamSubscriptionService.notifySubscribers(elements)
            Logger.info(s"saveEvents eventDao.insert: size: ${elements.size}")
            eventDao.insert(elements).map(completed => Logger.info(s"saveEvents standingDao.insert: size: ${elements.size} results: $completed"))
        }
        val update = mappedApiEvents.filter(mappedEvent => savedEvents.exists(savedEvent => mappedEvent == savedEvent && !mappedEvent.isTheSame(savedEvent))) match {
          case Nil => Future.successful()
          case elements =>
            teamSubscriptionService.notifySubscribers(elements)
            Logger.info(s"saveEvents eventDao.update: size: ${elements.size}")
            eventDao.update(elements).map(updateResults => Logger.info(s"saveEvents standingDao.update: size: ${elements.size} result: ${updateResults.mkString(",")}"))
        }
        val delete = savedEvents.diff(mappedApiEvents) match {
          case Nil => Future.successful()
          case elements =>
            Logger.info(s"saveEvents eventDao.delete: size: ${elements.size}")
            eventDao.delete(elements).map(deleteResults => Logger.info(s"saveEvents standingDao.delete: size: ${elements.size} result: ${deleteResults.mkString(",")}"))
        }
        Future.sequence(insert :: update :: delete :: Nil).map(_ => ())
      }
    }).flatten

}
