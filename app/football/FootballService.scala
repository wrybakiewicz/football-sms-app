package football

import dao.{CountryDao, EventDao, LeagueDao, StandingDao}
import dao.model.{Country, Event, League, Standing}
import javax.inject.Inject

import scala.concurrent.Future

class FootballService @Inject()(countryDao: CountryDao,
                      leagueDao: LeagueDao,
                      standingDao: StandingDao,
                      eventDao: EventDao) {

  def getCountries: Future[Seq[Country]] = countryDao.getAll

  def getLeagues(countryId: String): Future[Seq[League]] = leagueDao.getByCountryId(countryId)

  def getStandings(leagueId: String): Future[Seq[Standing]] = standingDao.getByLeagueId(leagueId)

  def getEvents: Future[Seq[Event]] = eventDao.getAll

}
