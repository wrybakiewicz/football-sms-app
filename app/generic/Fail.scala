package generic

case class Fail(message: String)

case class PhoneNumberNotFound()

case class SubscriptionNotFound()