package generic

import java.util.concurrent.TimeUnit

import com.mongodb.ConnectionString
import org.mongodb.scala._
import org.mongodb.scala.connection.NettyStreamFactoryFactory

class MongoDbConnector {

  lazy val database: MongoDatabase = connect()

  private val databaseName = "footballSmsDatabase"
  private val databasePassword = "ivAxW8AVnQBDXZze"

  private def connect(): MongoDatabase = {
    val connectionString: ConnectionString = new ConnectionString(s"mongodb+srv://guziec:$databasePassword@cluster1-wz7dx.mongodb.net/test?retryWrites=true")
    val settings: MongoClientSettings = MongoClientSettings.builder()
      .streamFactoryFactory(NettyStreamFactoryFactory())
      .applyToSslSettings(builder => builder.enabled(true))
      .applyConnectionString(connectionString)
      .applyToClusterSettings(builder => builder
        .serverSelectionTimeout(20, TimeUnit.SECONDS)
      )
      .build()
    MongoClient(settings).getDatabase(databaseName)
  }
}
