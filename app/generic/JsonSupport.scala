package generic

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import authentication.model.{FacebookLoginData, FacebookLogoutData, FacebookResponseData}
import football.api.model._
import notification.model.StandingWithSubscription
import spray.json._


trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val countryFormat: RootJsonFormat[CountryApi] = jsonFormat2(CountryApi)
  implicit val leagueFormat: RootJsonFormat[LeagueApi] = jsonFormat4(LeagueApi)
  implicit val standingFormat: RootJsonFormat[StandingApi] = jsonFormat8(StandingApi)
  implicit val eventFormat: RootJsonFormat[EventApi] = jsonFormat12(EventApi)
  implicit val facebookDataFormat: RootJsonFormat[FacebookLoginData] = jsonFormat3(FacebookLoginData)
  implicit val facebookResponseDataFormat: RootJsonFormat[FacebookResponseData] = jsonFormat2(FacebookResponseData)
  implicit val facebookLogoutDataFormat: RootJsonFormat[FacebookLogoutData] = jsonFormat1(FacebookLogoutData)
}