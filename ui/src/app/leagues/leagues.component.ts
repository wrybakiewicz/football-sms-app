import {Component, Input, OnInit} from '@angular/core';
import {AppService} from "../app.service";
import {League} from "../model/league.model";
import {ActivatedRoute} from "@angular/router";
import {UserData} from "../model/userData.model";
import {UserDataService} from "../userData.service";

@Component({
  selector: 'app-teams',
  templateUrl: './leagues.component.html'
})
export class LeaguesComponent implements OnInit {

  @Input() leagues: League[];
  private userData: UserData;

  constructor(private route: ActivatedRoute,
              private appService: AppService,
              private userDataService: UserDataService) {}

  ngOnInit() {
    this.userDataService.currentMessage.subscribe(userData => this.userData = userData);
    this.getLeagues();
  }

  public getLeagues() {
    const countryId = this.route.snapshot.paramMap.get('id');
    this.appService.getLeagues(countryId, this.userData.token).subscribe((leagues: League[]) => {
      this.leagues = leagues;
      console.log(leagues.map(league => league.leagueName))
    }, () => {
      console.log("FAIL")
    })
  }

}
