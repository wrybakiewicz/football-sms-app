import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {count, map} from 'rxjs/operators';
import { Observable } from 'rxjs/index';
import {SocialUser} from "angular-6-social-login";
import {Country} from "./model/country.model";
import {League} from "./model/league.model";
import {Standing} from "./model/standing.model";
import {UserData} from "./model/userData.model";
import {TeamSubscribtion} from "./model/teamSubscribtion.model";
import {PhoneNumber} from "./model/phoneNumber.model";
import {GetLeagueTable} from "./model/getLeagueTable.model";

/**
 * Class representing application service.
 *
 * @class AppService.
 */
@Injectable()
export class AppService {

  private loginFacebookUrl = '/api/facebook/login';
  private logoutFacebookUrl = '/api/facebook/logout';
  private getCountriesUrl = "/api/football/getCountries/";
  private getLeaguesUrl = "/api/football/getLeagues/";
  private getLeagueTableUrl = "/api/notification/getLeagueTable";
  private teamSubscribtionUrl = "/api/notification/subscribe";
  private teamUnubscribtionUrl = "/api/notification/unsubscribe";
  private savePhoneNumberUrl = "api/notification/savePhoneNumber";

  constructor(private http: HttpClient) {}

  public loginFacebook(user: SocialUser): Observable<any> {
    return this.http.post(this.loginFacebookUrl, user);
  }

  public logoutFacebook(logoutData: UserData): Observable<any> {
    return this.http.post(this.logoutFacebookUrl, logoutData);
  }

  public getCountries(token: String): Observable<Country[]> {
    return this.http.get<Country[]>(this.getCountriesUrl + token);
  }

  public getLeagues(countryId: string, token: String): Observable<League[]> {
    return this.http.get<League[]>(this.getLeaguesUrl + countryId + "/" + token);
  }

  public getLeagueTable(getLeagueTable: GetLeagueTable): Observable<Standing[]> {
    return this.http.post<Standing[]>(this.getLeagueTableUrl, getLeagueTable);
  }

  public subscribeTeam(teamSubscribtion: TeamSubscribtion): Observable<any> {
    return this.http.post(this.teamSubscribtionUrl, teamSubscribtion);
  }

  public unsubscribeTeam(teamSubscribtion: TeamSubscribtion): Observable<any> {
    return this.http.post(this.teamUnubscribtionUrl, teamSubscribtion);
  }

  public savePhoneNumber(number: PhoneNumber): Observable<any> {
    return this.http.post(this.savePhoneNumberUrl, number);
  }
}
