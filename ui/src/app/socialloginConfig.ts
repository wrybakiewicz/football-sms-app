import {AuthServiceConfig, FacebookLoginProvider} from "angular-6-social-login";

export function getAuthServiceConfigs()
{
  return new AuthServiceConfig([
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider("340873949621312")
    }
  ]);
}
