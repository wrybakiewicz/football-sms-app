import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LeaguesComponent} from "./leagues/leagues.component";
import {CountriesComponent} from "./countries/countries.component";
import {StandingsComponent} from "./teams/standings.component";


const routes: Routes = [
  {
    path: 'countries',
    component: CountriesComponent
  },
  {
    path: 'leagues/:id',
    component: LeaguesComponent
  },
  {
    path: 'teams/:leagueId',
    component: StandingsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
