import {Component, Input, OnInit} from '@angular/core';
import {AppService} from "../app.service";
import {League} from "../model/league.model";
import {ActivatedRoute} from "@angular/router";
import {Standing} from "../model/standing.model";
import {UserData} from "../model/userData.model";
import {UserDataService} from "../userData.service";
import {TeamSubscribtion} from "../model/teamSubscribtion.model";
import {PhoneNumber} from "../model/phoneNumber.model";
import {parseHostBindings} from "@angular/compiler";
import {GetLeagueTable} from "../model/getLeagueTable.model";

@Component({
  selector: 'app-leagues',
  templateUrl: './standings.component.html'
})
export class StandingsComponent implements OnInit {

  @Input() standings: Standing[];
  private userData: UserData;

  constructor(private route: ActivatedRoute,
              private appService: AppService,
              private userDataService: UserDataService) {}

  ngOnInit() {
    this.userDataService.currentMessage.subscribe(userData => this.userData = userData);
    this.getTeams();
  }

  public getTeams() {
    const leagueId = this.route.snapshot.paramMap.get('leagueId');
    const getLeagueTable: GetLeagueTable = {token: this.userData.token, leagueId: leagueId};
    this.appService.getLeagueTable(getLeagueTable).subscribe((standings: Standing[]) => {
      this.standings = standings.sort((standing1, standing2) => {
        if(Number(standing1.leaguePosition) > Number(standing2.leaguePosition)) return 1;
        else return -1;
      });
    }, () => {
      console.log("FAIL")
    })
  }

  public subscribe(teamName: string) {
    let standingIndex = this.standings.findIndex(standing => standing.teamName == teamName);
    this.standings[standingIndex].isSubscribing = true;

    const subscribtion: TeamSubscribtion = {teamName: teamName, token: this.userData.token};
    this.appService.subscribeTeam(subscribtion).subscribe(() => {
      console.log("OK");
    }, (error) => {
      if(error.status === 422) {
        this.standings[standingIndex].showPhoneInput = true;
      }
    });
  }

  public unsubscribe(teamName: string) {
    let standingIndex = this.standings.findIndex(standing => standing.teamName == teamName);
    this.standings[standingIndex].isSubscribing = false;

    const subscribtion: TeamSubscribtion = {teamName: teamName, token: this.userData.token};
    this.appService.unsubscribeTeam(subscribtion).subscribe(() => {

    });
  }

  public addPhoneNumber(teamName: string, phoneNumber: string) {
    const number: PhoneNumber = {phoneNumber: phoneNumber, token: this.userData.token};
    this.appService.savePhoneNumber(number).subscribe(() => {
      console.log("OK");
      let standingIndex = this.standings.findIndex(standing => standing.teamName == teamName);
      this.standings[standingIndex].showPhoneInput = false;
      this.subscribe(teamName)
    })
  }

}
