export class Standing {
  leagueId: string;
  teamName: string;
  leaguePosition: number;
  matchPlayed: number;
  matchWon: number;
  matchDraw: number;
  matchLost: number;
  points: number;

  isSubscribing: boolean;
  showPhoneInput: boolean;
}
