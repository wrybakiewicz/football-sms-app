export class League {
  leagueId: string;
  leagueName: string;
  countryId: string;
  countryName: string;
}
