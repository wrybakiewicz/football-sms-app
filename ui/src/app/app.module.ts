import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';

import { AppComponent } from './app.component';

import { AppService } from './app.service';
import { AppHttpInterceptorService } from './http-interceptor.service';
import { SocialLoginModule, AuthServiceConfig } from "angular-6-social-login";
import { getAuthServiceConfigs } from "./socialloginConfig";
import {CountriesComponent} from "./countries/countries.component";
import {LeaguesComponent} from "./leagues/leagues.component";
import {AppRoutingModule} from "./app-routing.module";
import {StandingsComponent} from "./teams/standings.component";
import {UserDataService} from "./userData.service";

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'Csrf-Token',
      headerName: 'Csrf-Token',
    }),
    AppRoutingModule,
    SocialLoginModule
  ],
  declarations: [
    AppComponent,
    CountriesComponent,
    LeaguesComponent,
    StandingsComponent
  ],
  providers: [
    AppService,
    UserDataService,
    {
      multi: true,
      provide: HTTP_INTERCEPTORS,
      useClass: AppHttpInterceptorService
    },
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
