import {Component, Input, OnInit} from '@angular/core';
import {Country} from '../model/country.model';
import {AppService} from "../app.service";
import {UserData} from "../model/userData.model";
import {UserDataService} from "../userData.service";

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html'
})
export class CountriesComponent implements OnInit {

  @Input() countries: Country[];
  private userData: UserData;

  constructor(private appService: AppService,
              private userDataService: UserDataService) {
  }

  ngOnInit() {
    this.userDataService.currentMessage.subscribe(userData => this.userData = userData);
    this.getCountries();
  }

  private getCountries() {
    this.appService.getCountries(this.userData.token).subscribe((countries: Country[]) => {
      this.countries = countries;
    }, () => {
      console.log("FAIL")
    });
  }

}
