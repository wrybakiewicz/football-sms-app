import {Component} from '@angular/core';

import {AppService} from './app.service';
import {AuthService, FacebookLoginProvider, SocialUser} from 'angular-6-social-login';
import {Router} from "@angular/router";
import {UserData} from "./model/userData.model";
import {UserDataService} from "./userData.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  private userLogged: boolean = false;
  private userData: UserData;

  constructor(private appService: AppService,
              private socialAuthService: AuthService,
              private router: Router,
              private userDataService: UserDataService) {
  }

  public facebookLogin() {
    let socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        this.loginFacebookInApp(userData)
      }
    );
  }

  public logout() {
    this.appService.logoutFacebook(this.userData).subscribe(() => {
      this.userLogged = false;
      this.socialAuthService.signOut();
      this.userDataService.changeMessage(new UserData())
    }, () => {
      this.userLogged = false;
      this.socialAuthService.signOut();
      this.userDataService.changeMessage(new UserData())
    });
  }

  private loginFacebookInApp(socialUser: SocialUser): void {
    this.appService.loginFacebook(socialUser).subscribe(() => {
        console.log("SUCCESS");
        this.userLogged = true;
        const userData: UserData = {token: socialUser.token};
        this.userData = userData;
        this.userDataService.changeMessage(userData);
        this.router.navigate(['/countries']);
      }, () => {
        console.log("FAIL")
      }
    );
  }

}
