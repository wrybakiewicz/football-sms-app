import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {UserData} from "./model/userData.model";

@Injectable()
export class UserDataService {

  private messageSource = new BehaviorSubject(new UserData());
  currentMessage = this.messageSource.asObservable();

  constructor() { }

  changeMessage(message: UserData) {
    this.messageSource.next(message)
  }

}
