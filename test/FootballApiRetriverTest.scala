import java.time.LocalDate

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import football.api.FootballApiRetriver
import org.scalatest.FreeSpec

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class FootballApiRetriverTest extends FreeSpec {

  implicit val system: ActorSystem = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  import system.dispatcher

  val retriver = new FootballApiRetriver()

  "getCountries" in {
    val countries = retriver.getAllCountries
    val result = Await.result(countries, Duration.Inf)
    println(result)
    assert(result.isRight)
  }

  "getLeague" in {
    val league = retriver.getLeagues("169")
    val result = Await.result(league, Duration.Inf)
    println(result)
    assert(result.isRight)
  }

  "getStandings" in {
    val standings = retriver.getStandings("62")
    val result = Await.result(standings, Duration.Inf)
    println(result)
    assert(result.isRight)
  }

  "getEvents - past" in {
    val events = retriver.getEvents("62", LocalDate.of(2015, 11, 11), LocalDate.now())
    val result = Await.result(events, Duration.Inf)
    println(result)
    assert(result.isRight)
  }

  "getEvents - future" in {
    val events = retriver.getEvents("62", LocalDate.now, LocalDate.of(2019, 3, 10))
    val result = Await.result(events, Duration.Inf)
    println(result)
    assert(result.isRight)
  }

}
